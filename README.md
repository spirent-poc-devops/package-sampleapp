# <img src="https://upload.wikimedia.org/wikipedia/en/thumb/c/ce/Spirent_logo.svg/490px-Spirent_logo.svg.png" alt="Spirent Logo" width="200"> <br/> SampleApp Deployment Using Custom Scripts

Sample application deployment package that uses custom scripts and kubernetes YAML files

# Use

Before installing the application you shall provision an environment using one of the available provisioning scripts:
* [Development Environment](https://gitlab.com/spirent-poc-devops/env-dev-ps)
* [Test Environment](https://gitlab.com/spirent-poc-devops/env-test-ps)
* [On-Premises Environment](https://gitlab.com/spirent-poc-devops/env-onprem-ps)
* [Cloud AWS Environment](https://gitlab.com/spirent-poc-devops/env-cloudaws-ps)

To install the application
```bash
./install_app.ps1 -Config <path to env config> [-Baseline <version>]
```

To upgrade the application
```bash
./upgrade_app.ps1 -Config <path to env config> [-Baseline <version>]
```

To uninstall the application
```bash
./uninstall_app.ps1 -Config <path to env config> [-Baseline <version>]
```

To generate a new baseline by tagging container in a target environment
```bash
./baseline_app.ps1 -Config <path to env config> -Baseline <version>
```

# Contacts

This sample was created by *Sergey Seroukhov*