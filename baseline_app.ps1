#!/usr/bin/env pwsh

param
(
    [Alias("c", "Path")]
    [Parameter(Mandatory=$true, Position=0)]
    [string] $ConfigPath,

    [Parameter(Mandatory=$true, Position=1)]
    [string] $Baseline = "",
    
    [Alias("r", "Resources")]
    [Parameter(Mandatory=$false, Position=2)]
    [string] $ResourcePath
)

# Stop on error
$ErrorActionPreference = "Stop"

# Load support functions
$path = $PSScriptRoot
if ($path -eq "") { $path = "." }
. "$($path)/common/include.ps1"
$path = $PSScriptRoot
if ($path -eq "") { $path = "." }

# Set default parameter values
if (($ResourcePath -eq $null) -or ($ResourcePath -eq ""))
{
    $ResourcePath = ConvertTo-EnvResourcePath -ConfigPath $ConfigPath
}

# Read config and resources
$config = Read-EnvConfig -ConfigPath $ConfigPath
$resources = Read-EnvResources -ResourcePath $ResourcePath

# Get launched containers images from all namespaces and select registry
$registry = Get-EnvMapValue -Map $config -Key "docker.registry"
Write-Host "Getting list of deployed components..."
$podsImages = kubectl describe pods --all-namespaces | Select-String -Pattern "Image:" | Select-String -Pattern $registry

# Login to docker server
Write-Host "Logging in to docker registry..."
docker login $registry -u $(Get-EnvMapValue -Map $config -Key "docker.user") -p $(Get-EnvMapValue -Map $config -Key "docker.pass")

foreach ($item in $podsImages) {
    [String]$podImage = $item
    # Save docker pod image and baseline image
    $dockerImage = $podImage.Substring($podImage.IndexOf("Image: ")+7).Trim()
    $dockerImageWithTag = $dockerImage
    # Remove tag from image if it exists
    if ($dockerImageWithTag.IndexOf(":") -gt 0) {
        $dockerImage = $dockerImage.Split(":")[0]
    }
    $baselineImage = $dockerImage + ":" + $Baseline

    # Pull image before tag
    Write-Host "Pulling '$dockerImageWithTag' image..."
    docker pull $dockerImageWithTag

    # Tag image with baseline tag
    docker tag $dockerImageWithTag $baselineImage

    # Push image to registry
    Write-Host "Pushing '$baselineImage' image..."
    docker push $baselineImage

    # Cleanup
    docker rmi $dockerImageWithTag --force
    docker rmi $baselineImage --force
}

docker image prune --force

Write-Host "All images from registry tagged with `"$Baseline`" successfully" -ForegroundColor Green