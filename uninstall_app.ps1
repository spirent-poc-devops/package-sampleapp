#!/usr/bin/env pwsh

param
(
    [Alias("c", "Path")]
    [Parameter(Mandatory=$true, Position=0)]
    [string] $ConfigPath,

    [Parameter(Mandatory=$false, Position=1)]
    [string] $Baseline = "latest",
    
    [Alias("r", "Resources")]
    [Parameter(Mandatory=$false, Position=2)]
    [string] $ResourcePath
)

# Stop on error
$ErrorActionPreference = "Stop"

# Load support functions
$path = $PSScriptRoot
if ($path -eq "") { $path = "." }
. "$($path)/common/include.ps1"
$path = $PSScriptRoot
if ($path -eq "") { $path = "." }

# Set default parameter values
if (($ResourcePath -eq $null) -or ($ResourcePath -eq ""))
{
    $ResourcePath = ConvertTo-EnvResourcePath -ConfigPath $ConfigPath
}

# Read config and resources
$config = Read-EnvConfig -ConfigPath $ConfigPath
$resources = Read-EnvResources -ResourcePath $ResourcePath
$namespace = "sampleapp"

# Define template parameters
$templateParams = @{
    namespace = $namespace
    baseline = $baseline
}

# Delete services
Build-EnvTemplate -InputPath "$($path)/templates/services.yml" -OutputPath "$($path)/temp/services.yml" -Params1 $templateParams
kubectl delete -f "$($path)/temp/services.yml"

# Delete pods
Build-EnvTemplate -InputPath "$($path)/templates/pods.yml" -OutputPath "$($path)/temp/pods.yml" -Params1 $templateParams
kubectl delete -f "$($path)/temp/pods.yml"

# Delete connection-params config map
kubectl delete configmap connection-params -n $namespace

# Delete connection-creds secret
kubectl get secret connection-creds -n $namespace

# Delete namespace
Build-EnvTemplate -InputPath "$($path)/templates/namespace.yml" -OutputPath "$($path)/temp/namespace.yml" -Params1 $templateParams
kubectl delete -f "$($path)/temp/namespace.yml"
