$path = $PSScriptRoot
if ($path -eq "") { $path = "." }

function Read-EnvConfig
{
    [CmdletBinding()]
    param (
        [Parameter(Mandatory=$true, Position=0)]
        [string] $ConfigPath
    )

    if (($ConfigPath -eq $null) -or ($ConfigPath -eq "")) {
        throw "Config path is not set. Execute <script>.ps1 -Config <config file>"
    }

    if (-not (Test-Path -Path $ConfigPath)) {
        throw "Config path $($ConfigPath) was not found"
    }

    # Read the config file
    # $Config = Get-Content -Path $ConfigPath | ConvertFrom-Json | ConvertTo-Hashtable
    $config = Get-Content -Path $ConfigPath | Out-String | ConvertFrom-Json | ConvertTo-EnvHashtable

    # Apply default configuration
    $defaultConfigPath = "$path/../config/default_config.json"
    if (Test-Path -Path $defaultConfigPath)
    {
        $defaultConfig = Get-Content -Path $defaultConfigPath | Out-String | ConvertFrom-Json | ConvertTo-EnvHashtable
        Set-EnvMapDefault -Map $config -Default $defaultConfig
    }

    Write-Output $config
}

function ConvertTo-EnvResourcePath
{
    [CmdletBinding()]
    param (
        [Parameter(Mandatory=$true, Position=0)]
        [string] $ConfigPath
    )

    $parent = Split-Path -Path $ConfigPath -Parent
    $file = Split-Path -Path $ConfigPath -Leaf
    $lastDotPos = $file.LastIndexOf('.')

    if($lastDotPos -gt -1)
    {
        $file = $file.Substring(0, $lastDotpos)
    }

    if ($file.Contains("config")) {
        $file = $file.Replace("config", "resources")
    } else {
        $file = $file + "_resources"
    }

    return $parent + "/" + $file + ".json"
}

function Read-EnvResources
{
    [CmdletBinding()]
    param (
        [Parameter(Mandatory=$true, Position=0)]
        [string] $ResourcePath
    )
    
    # If resources do not exist yet return an empty result
    if (($ResourcePath -eq $null) -or ($ResourcePath -eq "")) {
        return @{}
    }
    if (-not (Test-Path -Path $ResourcePath)) {
        return @{}
    }

    # Read the resources file
    # $Resources  = Get-Content -Path $Path | ConvertFrom-Json | ConvertTo-Hashtable
    $Resources = Get-Content -Path $ResourcePath | Out-String | ConvertFrom-Json | ConvertTo-EnvHashtable
    Write-Output $Resources
}

function Write-EnvResources
{
    [CmdletBinding()]
    param (
        [Parameter(Mandatory=$true, Position=0)]
        [string] $ResourcePath,
        [Parameter(Mandatory=$true, Position=1)]
        [hashtable] $Resources
    )

    # Write the content to resource file
    $Content = ConvertTo-Json $Resources
    Set-Content -Path $ResourcePath -Value $Content
}



function Log-Error{
    [CmdletBinding()]
    param (
        
        [Parameter(Mandatory=$true, Position=1)]
        [string] $Message
    )
    $component=$env:LogComponent
    Log-Data -Level "Error" -Message $Message -Component $component
}

function Log-Info{
    [CmdletBinding()]
    param (
        
        [Parameter(Mandatory=$true, Position=1)]
        [string] $Message
    )
    $component=$env:LogComponent
    Log-Data -Level "Info" -Message $Message -Component $component

}

function Log-Warn{
    [CmdletBinding()]
    param (
        
        [Parameter(Mandatory=$true, Position=1)]
        [string] $Message
    )
    
    $component=$env:LogComponent
    Log-Data -Level "Warn" -Message $Message -Component $component

}

function Log-Debug{
    [CmdletBinding()]
    param (
        [Parameter(Mandatory=$true, Position=1)]
        [string] $Message
    )
    $component=$env:LogComponent
    Log-Data -Level "Debug" -Message $Message -Component $component

}
function Log-Data
{
    [CmdletBinding()]
    param (
        [Parameter(Mandatory=$true, Position=0)]
        [ValidateSet("Warn","Error","Info","Debug")]
        [string] $Level,
        [Parameter(Mandatory=$true, Position=1)]
        [string] $Message,
        [Parameter(Mandatory=$true, Position=1)]
        [string] $Component
    )


    $rootPath = $PSScriptRoot
    $fileName=$env:LogFileName
    if (($fileName -eq $null) -or ($fileName -eq "")){
        $path="$($rootPath)/../logs/dev_env.log"
        # Create the file if it doesn't exist
        if(!(Test-Path $path)){
            $null > $path
        }
    }else{
        $path="$($rootPath)/../logs/$fileName"
    }

    # Write-Host $path
    # If resources do not exist yet return an empty result
    if (($Level -eq $null) -or ($Message -eq "")) {
        Write-Output "Missing the log level or message"
    }
    
    $logData = Get-Content $path | Out-String
    $timeStamp = (Get-Date).toString("yyyy/MM/dd HH:mm:ss")

    $Line = "{`"component`":`"$Component`",`"timestamp`":`"$timeStamp`",`"level`": `"$Level`",`"message`":`"$Message`"}"
    Add-content $path -value $Line   
}


function Create-LogFile{
    $rootPath = $PSScriptRoot
    $logTime = (Get-Date).toString("yyyy-MM-dd_hh-mm-ss")
    $fileName="env"+"_"+$logTime+".log"
    $path="$($rootPath)/../logs/$fileName"
    New-Item -Path $path -ItemType File
    $env:LogFileName = $fileName
}

function Get-LogFile{
    $path="$PSScriptRoot/../logs"
    $LatestLogFile=Get-ChildItem $path | Sort-Object -Descending -Property LastWriteTime -Top 1
    $env:LogFileName = $LatestLogFile.Name
}





